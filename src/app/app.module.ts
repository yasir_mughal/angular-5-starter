import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './user_auth/login.component';
import { SignupComponent } from './user_auth/signup.component';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import {routing} from './app.routing';
import {RouterGuard} from './app.router.guard';
import { HomeComponent } from './home/home.component';
import {LocalStorageService} from './shared/services/local-storage.service';
import {ApiService} from './shared/services/api.service';
import {SharedModule} from './shared/module/shared.module';
import { LoginHeaderComponent } from './header/login-header/login-header.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DropdownDirective,
    HomeComponent,
    LoginHeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    SharedModule
  ],
  providers: [RouterGuard, LocalStorageService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
