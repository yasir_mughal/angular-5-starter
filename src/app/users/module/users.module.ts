import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserComponent} from '../user.component';
import {usersRouting} from '../routing/users.routing';

@NgModule({
  imports: [
    CommonModule,
    usersRouting
  ],
  declarations: [UserComponent]
})
export class UsersModule { }
