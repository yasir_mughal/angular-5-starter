import {Routes, RouterModule} from '@angular/router';
import {UserComponent} from '../user.component';

const USER_ROUTES: Routes = [
  {path: '', component: UserComponent, children: [
  ]}
];

export const usersRouting = RouterModule.forChild(USER_ROUTES);
